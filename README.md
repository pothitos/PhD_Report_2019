# Annual PhD Studies Report 2019

This repository produces a
[PDF](https://gitlab.com/pothitos/PhD_Report_2019/-/jobs/artifacts/master/file/PhD_Report_2019.pdf?job=build)
with my annual report for the progress of my PhD studies.
This is requested by my Department of Informatics and
Telecommunications according to a
[template](http://www.di.uoa.gr/sites/default/files/YPOMNHMA_ETHSIAS_PROODOY_YPOPSIFIOY_DIDAKTORA.docx).

---

An [Open
Research](https://gist.github.com/pothitos/ec5f4f66ddd113aea6bac4094690d72e)
work
